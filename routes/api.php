<?php

use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('schedules', 'ScheduleController@index');

/* Route::resource('test', 'CrudController'); 
// dicomment karena route:list tidak muncul karena route ini. 
// In CrudController.php line 19:
// Class name must be a valid object or a string */

Route::get('production_results/columns', 'ProductionResultController@getColumns');
Route::get('production_results/columnlist', 'ProductionResultController@getColumnList');
Route::get('production_results/required_columns', 'ProductionResultController@getRequiredColumn');
Route::resource('production_results', 'ProductionResultController');

Route::get('change_model_reports/columns', 'ChangeModelReportController@getColumns');
Route::get('change_model_reports/columnlist', 'ChangeModelReportController@getColumnList');
Route::get('change_model_reports/required_columns', 'ChangeModelReportController@getRequiredColumn');
Route::resource('change_model_reports', 'ChangeModelReportController');

Route::get('trial_ng_sets/columns', 'TrialNgSetController@getColumns');
Route::get('trial_ng_sets/columnlist', 'TrialNgSetController@getColumnList');
Route::get('trial_ng_sets/required_columns', 'TrialNgSetController@getRequiredColumn');
Route::resource('trial_ng_sets', 'TrialNgSetController');

Route::get('balance_set_conditions/columns', 'BalanceSetConditionController@getColumns');
Route::get('balance_set_conditions/columnlist', 'BalanceSetConditionController@getColumnList');
Route::get('balance_set_conditions/required_columns', 'BalanceSetConditionController@getRequiredColumn');
Route::resource('balance_set_conditions', 'BalanceSetConditionController');

Route::get('loss_time_reports/columns', 'LossTimeReportController@getColumns');
Route::get('loss_time_summaries', 'LossTimeReportController@getSummary');
Route::get('loss_time_summaries/required_columns', 'LossTimeReportController@requiredColSummary');
Route::get('loss_time_reports/columnlist', 'LossTimeReportController@getColumnList');
Route::get('loss_time_reports/required_columns', 'LossTimeReportController@getRequiredColumn');
Route::resource('loss_time_reports', 'LossTimeReportController');
Route::get('part_edi', 'LossTimeReportController@getPartEdi');

Route::get('change_member_reports/columns', 'ChangeMemberReportController@getColumns');
Route::get('change_member_reports/columnlist', 'ChangeMemberReportController@getColumnList');
Route::get('change_member_reports/required_columns', 'ChangeMemberReportController@getRequiredColumn');
Route::resource('change_member_reports', 'ChangeMemberReportController');

Route::get('indirect_lost_times/columns', 'IndirectOpHoursLostTimeController@getColumns');
Route::get('indirect_lost_times/columnlist', 'IndirectOpHoursLostTimeController@getColumnList');
Route::get('indirect_lost_times/required_columns', 'IndirectOpHoursLostTimeController@getRequiredColumn');
Route::resource('indirect_lost_times', 'IndirectOpHoursLostTimeController');

Route::get('indirect_op_hours/columns', 'IndirectOpHourController@getColumns');
Route::get('indirect_op_hours/columnlist', 'IndirectOpHourController@getColumnList');
Route::get('indirect_op_hours/required_columns', 'IndirectOpHourController@getRequiredColumn');
Route::resource('indirect_op_hours', 'IndirectOpHourController');

Route::get('actual_direct_op_hours/columns', 'ActualDirectOpHourController@getColumns');
Route::get('actual_direct_op_hours/columnlist', 'ActualDirectOpHourController@getColumnList');
Route::get('actual_direct_op_hours/required_columns', 'ActualDirectOpHourController@getRequiredColumn');
Route::post('actual_direct_op_hours/import_from_prod_res', "ActualDirectOpHourController@import_from_prod_res");
Route::resource('actual_direct_op_hours', 'ActualDirectOpHourController');

Route::get('test', 'TestController@test');

Route::resource('trial_ng_sets', 'TrialNgSetController');
Route::resource('change_member_reports', 'ChangeMemberReportController');
Route::resource('balance_set_conditions', 'BalanceSetConditionController');

Route::get('operation_summaries/columns', 'OperationSummaryController@getColumns');
Route::get('operation_summaries/columnlist', 'OperationSummaryController@getColumnList');
Route::get('operation_summaries/required_columns', 'OperationSummaryController@getRequiredColumn');
Route::resource('operation_summaries', 'OperationSummaryController');

// ReportHeaderController
Route::get('/report_headers', 'ReportHeaderController@index');
Route::get('/report_headers/{id}', 'ReportHeaderController@show');
Route::post('report_headers', 'ReportHeaderController@store');
Route::put('/report_headers/{id}', 'ReportHeaderController@update');

// Approval Process by Zaki
Route::get('/report_header_lists', 'ReportHeaderListController@index'); //-- Report header List
Route::resource('admin_production_results', 'AdminProductionResultController'); // -- Production Results
Route::resource('admin_loss_times', 'AdminLossTimeReportController'); // -- Loss Times
Route::resource('admin_change_model_reports', 'AdminChangeModelReportController'); // -- Loss Results
Route::resource('admin_trial_ng_sets', 'AdminTrialNgSetController'); // -- Trial NG Set
Route::resource('admin_change_member_reports', 'AdminChangeMemberReportController'); // -- Change Member Report
Route::resource('admin_balance_set_conditions', 'AdminBalanceSetConditionController'); // -- Balance Set Condition
Route::resource('admin_operation_summaries', 'AdminOperationSummaryReportController'); // -- Operation Summary
// Route::get('admin_indirect_oph_lost_times/{id}/required_columns', 'LossTimeReportController@getRequiredColumn');
Route::get('admin_indirect_oph_lost_times/{id}', 'LossTimeReportController@getIndOphLostTime'); // -- Indirect Lost Time
Route::get('admin_indirect_oph_lost_times/{id}/required_columns', 'LossTimeReportController@getRequiredColumnManual');
Route::get('listapproval/{id}', 'ApprovalController@getListApproval'); // -- Menampilkan data yang Approve pada Report ID Tersebut

Route::get('/master_loss_time/combo', 'MasterLossTimeController@combo');
Route::get('/dic/combo', 'DicController@combo');
Route::get('/lines/combo', 'LineController@combo');
Route::get('/master_indirect_op_hours/combo', 'MasterIndirectOpHourController@combo');
Route::get('/master_categories/combo', 'MasterCategoryController@combo');

Route::get('/dashboard/summary_perline', 'DashboardController@summaryPerLine');
Route::get('/dashboard/productivity_per_date', 'DashboardController@productivityByDate');
Route::get('/dashboard/efficiency_per_date', 'DashboardController@efficiencyPerDate');
Route::get('/dashboard/loss_time_per_dept', 'DashboardController@loss_time_per_dept');
Route::get('/dashboard/least_avg_efficiency_perline', 'DashboardController@least_avg_efficiency_perline');
Route::get('/dashboard/least_avg_productivity_perline', 'DashboardController@least_avg_productivity_perline');
Route::get('/dashboard/least_avg_rate_of_attendance_perline', 'DashboardController@least_avg_rate_of_attendance_perline');
Route::get('/dashboard/least_avg_rate_of_operation_perline', 'DashboardController@least_avg_rate_of_operation_perline');
Route::get('/dashboard/loss_time_per_responsible', 'DashboardController@loss_time_per_responsible');
Route::get('/dashboard/loss_time_per_line', 'DashboardController@loss_time_per_line');

Route::get('/configs', "ConfigController@index");