@extends('voyager::master')
<link rel="stylesheet" type="text/css" href="{{ asset('/css/jam.css') }}">

@section('page_header')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <h4>Change Member Report </h4>
                <button style="margin-bottom:10px;" type="button" class="btn btn-success voyager-plus pull_right" data-toggle="modal" data-target="#exampleModal">
                 Add New</button>
               <a class="btn btn-danger" id="bulk_delete_btn"><i class="voyager-trash pull_right"></i> <span>Bulk Delete</span></a>
              
            </div>
        </div>   
        
    </div>
@stop 

@section('content')
<div class="container-fluid">
<!-- Button trigger modal -->
    <!-- Modal add -->
    <div class="modal modal-primary fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center" id="exampleModalLabel">INPUT CHANGE MEMBER REPORT</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="change_member/create_new_report" method="post">
                    {{ csrf_field() }}
                        <label for=""><b> Report id</b></label>
                        <select class="form-control select1" name="report_id">
                            @foreach($dropdown as $dd)
                            <option value="{{$dd->id}}">{{$dd->id}}</option>
                            @endforeach
                        </select>
                       
                        <label for=""><b>Flag</b></label>
                        <input type="text" name="flag" class="form-control" id="exampleInputEmail1" placeholder="">
                        <label for=""><b>Shift </b></label>
                        <select class="form-control" name="shift" id="exampleFormControlSelect1">
                           
                            <option>Shift 1</option>
                            <option>Shift 2</option>
                            <option>Shift 3</option>
                        </select>
                        <label for=""><b></b>Refrence No</label>
                        <input type="text" name="refrence_no" class="form-control" id="exampleInputEmail1" placeholder=" ">
                        <label for=""><b></b>Remark</label>
                        <input type="text" name="remark" class="form-control" id="exampleInputEmail1" placeholder=" ">
    
                        <!-- <label for="">Create User</label>  -->
                        <input readonly class="form-control" name="create_user" type="hidden" value="{{ app('VoyagerAuth')->user()->name }}">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <br>
    <small>Prodection Change Member Report</small>
    <div class="panel panel-default">
        <div class="panel-body">
            <!-- search -->
            <form method="get" class="form-search">
                <div id="search-input">
                    <select id="search_key" name="search" tabindex="-1" class="select2-hidden-accessible" aria-hidden="true">
                        <option value="id">Id</option>
                        <option value="flag">Flag</option>
                        <option value="shift">Shift</option>
                        <option value="refrence_no">Refrence_no</option>
                        <option value="remark">Remark</option>
                        <option value="create_time">Create Time</option>
                        <option value="create_user">User Create</option>
                    </select>
                    <span class="select2 select2-container select2-container--default" dir="ltr" style="width: 100px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-search_key-container"><span class="select2-selection__rendered" id="select2-search_key-container" title="Id">Id</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                    <select id="filter" name="filter" tabindex="-1" class="select2-hidden-accessible" aria-hidden="true">
                        <option value="contains">contains</option>
                        <option value="equals">=</option>

                    </select>
                    <span class="select2 select2-container select2-container--default" dir="ltr" style="width: 100px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-filter-container"><span class="select2-selection__rendered" id="select2-filter-container" title="contains">contains</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                        <div class="input-group col-md-12">
                        <input type="text" class="form-control" placeholder="Search" name="s" value="">
                    <span class="input-group-btn">
                                <button class="btn btn-info btn-lg" type="submit">
                                    <i class="voyager-search"></i>
                                </button>
                            </span>
                        </div>
                </div>
            </form>
        <!-- form table -->
            <table class="table table-hover">
                <thead>
                    <tr>
                    <tr>
                        <th>
                            <input type="checkbox" class="select_all">
                        </th>
                        <th>Report id</th>
                        <th>Flag</th>
                        <th>Shift</th>
                        <th>Refrence No</th>
                        <th>Remark</th>
                        
                        <th>Create User</th>
                        <th>Created_at</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $datas)
                    <tr>
                        <td>
                            <input type="checkbox" name="row_id" id="checkbox_2" value="2">
                        </td>
                        <td>{{ $datas->member->id }}</td>
                        <td>{{ $datas->flag }}</td>
                        <td>{{ $datas->shift }}</td>
                        <td>{{ $datas->refrence_no}}</td>
                        <td>{{ $datas->remark }}</td>
                        <td>{{ $datas->create_user }}</td>
                        <td>{{date('d M Y',strtotime($datas->created_at))}}</td>
                        <td>
                        <!-- <button style="margin-bottom:10px;" type="button" class="btn btn-primary voyager-eye pull-left view" data-toggle="modal" data-target="#exampleModalView"></button> -->
                        <a href="change_member/view/{{ $datas->id }}" class="btn btn-warning btn-sm pull-left voyager-eye"></button>
                        <a href="change_member/edit/{{ $datas->id }}" class="btn btn-primary btn-sm pull-left voyager-edit"></button>
                        <a href="change_member/delete/{{ $datas->id}}" onClick="return confirm('Are you sure, you want to delete this line?')" class="btn btn-danger voyager-trash pull-left"></a>
                     </td>

                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

<!-- modal view -->
<div class="modal modal-primary  fade" id="exampleModalView" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center" id="exampleModalLabel">DETAIL CHANGE MEMBER REPORT </h4>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                     <div class="row">
                         <div class="col-lg-12">
                            <table class="table table_bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Report id</th>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>Flag</th>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>Shift</th>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>Refrence No</th>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>Remark</th>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>Create_user</th>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th>Created_at</th>
                                        <td></td>
                                    </tr>
                                  
                                </thead>
                            </table>
                         </div>
                    </div>
            </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
<!-- Button trigger modal -->
    <!-- Modal update -->
    <div class="modal modal-primary fade" id="exampleModalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center" id="exampleModalLabel">UPDATE CHANGE MEMBER REPORT</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="admin/change_member/update_member_report/{id}" method="post">
                    {{ csrf_field() }}
                        <label for=""><b> Report id</b></label>
                        <select  value="" class="form-control select1" name="report_id">
                            @foreach($dropdown as $dd)
                            <option value="{{$dd->id}}">{{$dd->id}}</option>
                            @endforeach
                        </select>
                       
                        <label for=""><b>Flag</b></label>
                        <input  value="" type="text" name="flag" class="form-control" id="exampleInputEmail1" placeholder="">
                        <label for=""><b>Shift </b></label>
                        <select  value="" class="form-control" name="shift" id="exampleFormControlSelect1">
                           
                            <option>Shift 1</option>
                            <option>Shift 2</option>
                            <option>Shift 3</option>
                        </select>
                        <label for=""><b></b>Refrence No</label>
                        <input  value="" type="text" name="refrence_no" class="form-control" id="exampleInputEmail1" placeholder=" ">
                        <label for=""><b></b>Remark</label>
                        <input  value="" type="text" name="remark" class="form-control" id="exampleInputEmail1" placeholder=" ">    
                        <input readonly class="form-control" name="create_user" type="hidden" value="{{ app('VoyagerAuth')->user()->name }}">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Change Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
@endsection