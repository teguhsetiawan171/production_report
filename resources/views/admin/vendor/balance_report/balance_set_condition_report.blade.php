@extends('voyager::master')

@section('page_header')
<Div class="container-fluid">
    <div class="panel-body">
        <h4>Balance Set Condition Report</h4>
    </div>
</Div>
<br>
<br>
<br>
@stop 

@section('content')
    <div class="container">
    <!-- Button trigger modal -->
        
        <div class="row">
            
            <div class="col-md-8">
                <form action="/admin/balance_set_condition_report" method="get">
                    <div class="input-group">
                        <span class="input-group-prepend">
                            <button type="submit" class="btn btn-primary btn-sm">Search</button>
                            <a href="/admin/balance_set_condition_report" class="btn btn-danger btn-sm" >Reset</a>
                        </span>
                        <input type="search" name="search" value="{{$search}}" class="form-control">
                       
                    </div>
                </form>
            </div>
            <div class="col-md-4">
                <button type="button" style="position: absolute;left: 170px;top: 45px;"  class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                Tambah Data
                </button>
            </div>
        </div>
        

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">ADD NEW</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="balance_set_condition_report/create_new_report" method="post">
                    {{ csrf_field() }}
                        <label for=""><b> Report ID</b></label>
                            <select class="form-control" name="report_id">
                                @foreach($data2 as $dd)
                                <option value="{{$dd->id}}">{{$dd->id}}</option>
                                @endforeach
                                </select>

                        <div class="form-group" style="margin-top:10px;">
                            <label for="exampleFormControlSelect1">Model</label>
                            <select class="form-control" name="model" id="exampleFormControlSelect1">
                            <option value="ABC">ABC</option>
                            <option value="DEF">DEF</option>
                            <option value="GHI">GHI</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">QTY</label>
                            <input required type="number" class="form-control" id="exampleInputPassword1" name="qty">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Remarks</label>
                            <textarea required name="remarks" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Shift</label>
                            <select class="form-control" name="shift" id="exampleFormControlSelect1">
                            <option value="normal shift" >Normal-Shift</option>
                            <option value="s-A">Shift-A</option>
                            <option value="s-B">Shift-B</option>
                            <option value="s-C">Shift-C</option>
                            </select>
                        </div>
                        <input readonly type="hidden" name="user_create" value="{{ app('VoyagerAuth')->user()->name }}">
                        <input readonly type="hidden" name="created_at" value="<?php echo date ("d-m-y H:i:s") ?>">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
       <div class="panel-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Model</th>
                        <th>QTY</th>
                        <th>Remarks</th>
                        <th>Shift</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $dd)
                    <tr>
                        <td> {{$dd->model}} </td>
                        <td> {{$dd->qty }} </td>
                        <td> {{$dd->remarks }} </td>
                        <td> {{$dd->shift }} </td>
                        <td>
                            <a href="balance_set_condition_report/view/{{$dd->id}}" class="btn btn-success btn-sm voyager-eye"></a>
                            <a href="balance_set_condition_report/edit/{{$dd->id}}" class="btn btn-info btn-sm voyager-edit"></a>
                            <a href="balance_set_condition_report/delete/{{$dd->id}}" class="btn btn-danger btn-sm voyager-x" onclick="return confirm('Anda yakin mau menghapus item ini ?')"></a>
                            
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            
    {{ $data->links() }}
        </div>
    </div>
    


@endsection