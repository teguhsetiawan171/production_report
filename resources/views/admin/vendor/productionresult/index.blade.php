@extends('voyager::master')
<link rel="stylesheet" type="text/css" href="{{ asset('/css/jam.css') }}">

@section('page_header')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <h4>Change Production Result</h4>
                <button style="margin-bottom:10px;" type="button" class="btn btn-success voyager-plus pull_right" data-toggle="modal" data-target="#exampleModal">
                 Add New</button>
            </div>
        </div>   
        
    </div>
@stop 
@section('content')
<div class = "container-fluid">
    <!-- Modal add -->
    <div class="modal modal-primary fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">PRODUCTION RESULT</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="production/create_new_production" method="post">
                    {{ csrf_field() }}
                        <label for=""><b>Report id</b></label>
                        <select class="form-control select1" name="report_id">
                            @foreach($dropdown as $dd)
                            <option value="{{$dd->id}}">{{$dd->id}}</option>
                            @endforeach
                        </select>
                          
                        <label for=""><b>Model</b></label>
                        <input type="text" name="model" class="form-control" id="exampleInputEmail1">

                        <label for=""><b>Lot Number</b></label>
                        <input type="text" name="lot_no" class="form-control" id="exampleInputEmail1">

                        <label for=""><b>Lot Qty</b></label>
                        <input type="text" name="lot_qty" class="form-control" id="exampleInputEmail1">
                        <!-- <label for="">Create User </label> -->
                        <input readonly class="form-control" name="user_create" type="hidden" value="{{ app('VoyagerAuth')->user()->name }}">

                        </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <br>
    <!-- table -->
    <small>Prodection Production Result</small>
    <div class="panel panel-default">
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Report id</th>
                        <th>Model</th>
                        <th>Lot No</th>
                        <th>Lot Quantity</th>
                        <th>Create time</th>
                        <th>Create User</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $datas)
                    <tr>
                        <td>{{ $datas->produk->id }}</td>
                        <td>{{ $datas->model }}</td>
                        <td>{{ $datas->lot_no }}</td>
                        <td>{{ $datas->lot_qty }}</td>
                        <td>{{date('d M Y',strtotime($datas->create_time))}}</td>
                        <td>{{ $datas->user_create }}</td>
                        <td>
                            <!-- <button href="" class="btn btn-primary btn-sm pull-left voyager-eye"></button> -->
                            <a href="production/view/{{ $datas->id }}" class="btn btn-warning btn-sm pull-left voyager-eye"></button>
                            <a href="production/edit/{{ $datas->id }}" class="btn btn-primary btn-sm pull-left voyager-edit"></button>
                            <a href="production/delete/{{ $datas->id }}"onClick="return confirm('Are you sure, you want to delete this line?')"  class="btn btn-danger pull-left voyager-trash"></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

</div>
@endsection