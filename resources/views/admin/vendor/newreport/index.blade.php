@extends('voyager::master')

@section('page_header')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <h4>Create A new Report</h4>
            </div>
        </div>
        
    </div>
@stop 

@section('content')
<div class="container-fluid">
<!-- Button trigger modal -->
    <button style="margin-bottom:20px;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
    Add New Data
    </button>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">PILIH</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="report_header/create_new_report" method="post">
                    {{ csrf_field() }}
                        <label for=""><b> Line</b></label>
                        <select class="form-control" name="line">
                            @foreach($dropdown as $dd)
                            <option value="{{$dd->id}}">{{$dd->line}}</option>
                            @endforeach
                        </select>

                        <input readonly class="form-control" name="user_create" type="hidden" value="{{ app('VoyagerAuth')->user()->name }}">

                        <input type="text" name="approve_id" class="form-control" value="{{$no_approval}}" style="margin-top:10px;"readonly>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- endmodal -->

    <br>
    <small>Prodection Report Data</small>
    <div class="panel panel-default">
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>LINE</th>
                        <th>CREATE TIME</th>
                        <th>CREATE BY</th>
                        <th>ACTION</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $datas)
                    <tr>
                        <td>{{ $datas->line_r->line }}</td>
                        <td>{{date('d M Y',strtotime($datas->created_at))}}</td>
                        <td>{{ $datas->user_create }}</td>
                        <td>
                            <a href="report_header/edit/{{$datas->id}}" class="btn btn-info btn-sm">view</a>
                            <a href="admin/report_header/delete/{{ $datas->id }}" class="btn btn-danger btn-sm" onclick="return confirm('Anda yakin mau menghapus item ini ?')">Delete</a>
                            <a href="./admin/"></a>
                        </td>

                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
