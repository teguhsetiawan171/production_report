@extends('voyager::master')

@section('content')
<div id="app">
  <main-component></main-component>
</div>
@endsection



@section('javascript')

<script src="{{url('js/approval-config.js')}}"></script>
@endsection