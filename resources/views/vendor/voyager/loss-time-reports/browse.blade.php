@extends('voyager::master')

@section('content')
<div id="app">
  <router-view></router-view>
</div>
@endsection



@section('javascript')

<script src="{{url('js/lost_time_reports.js')}}"></script>
<script>
    $(".profile").on('click',function(){
    if($( ".profile" ).index('.open') == 0){
      $(".profile").removeClass("open");
    }
    else{
      $(".profile").addClass("open");
    }
  });
</script>
@endsection