import $ from 'jquery';

$(document).ready(function(){
    console.log('document ready');
    $('form#op_summary_form').on('submit', function(e) {
        e.preventDefault();
        let url = $(this).prop('action');
        let lines = $('#op_summary_lines').val();
        let month = $('#op_summary_month').val();

        lines.forEach( line => {
            // buka new tab
            let params = {line, month};
            let fullUrl = url +"?"+ $.param(params);
            // console.log({params, url, fullUrl});
            window.open(fullUrl, '_blank');

        });
    })
})