<?php

namespace App\Http\Controllers;
use App\ChangeMemberReport;
use App\ReportsHeader;
use Illuminate\Http\Request;
use DB;
class CMRController extends Controller
{

    public function index(){
        $data = ChangeMemberReport::orderBy('id','desc')->get();
        $dropdown = ReportsHeader::get();
        return view('admin.vendor.changemember.index',compact('data','dropdown'));
    }
    public function create(Request $request){
        ChangeMemberReport ::create($request->all());
        return redirect('/admin/change_member');
    }
    public function view($id){
        $data = ChangeMemberReport::findOrFail($id);
        return view('admin.vendor.changemember.view',compact('data'));
    }
    public function destroy($id){
        \Session::flash('flash_message','data berhasil di hapus');
        ChangeMemberReport::where('id',$id)->delete();
        return redirect('/admin/change_member');
    } 
    public function edit($id){
        $data=ChangeMemberReport::findOrFail($id);
        return view('admin.vendor.changemember.edit',compact('data'));
    }
    public function update($id, Request $request)
    {
        $member =ChangeMemberReport::findorFail($id);
        $member->report_id = $request->report_id;
        $member->flag = $request->flag;
        $member->shift = $request->shift;
        $member->refrence_no = $request->refrence_no;
        $member->remark = $request->remark;
        $member->save();
        return redirect('/admin/change_member');
    }
}
