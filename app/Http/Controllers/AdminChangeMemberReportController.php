<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminChangeMemberReportController extends CrudAdminController
{
    protected $model = "App\\ChangeMemberReport";
}
