<?php

namespace App\Http\Controllers;

use App\Line;
use App\LossTimeReport;
use App\OperationSummary;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    //
    public function summaryPerLine(Request $request)
    {
        $query = DB::table('operation_summaries')->select([
            DB::raw("AVG( efficiency) as efficiency"),
            DB::raw("AVG( rate_of_operation) as rate_of_operation"),
            DB::raw(
                "AVG( productivity) as productivity "
            ),
            DB::raw(
                "AVG( rate_of_attendance) as rate_of_attendance"
            ),
            "report_headers.line",
            "report_headers.report_date"
        ])->join("report_headers", "report_headers.id", "=", "operation_summaries.report_header_id")
            ->groupBy([
                "line", "report_headers.report_date"
            ]);

        if ($request->has('line')) {
            $query->where('report_headers.line', '=', $request->line);
        }

        if ($request->has('report_date')) {
            $query->where('report_headers.report_date', '=', $request->report_date);
        }


        $queryResult = $query->get();

        $results = collect([
            "request" => $request->all()
        ]);


        return $results->merge([
            "data" => $queryResult
        ]);
    }

    public function productivityByDate(Request $request)
    {
        $query = DB::table('operation_summaries')->select([
            DB::raw("AVG( efficiency) as efficiency"),
            DB::raw("AVG( rate_of_operation) as rate_of_operation"),
            DB::raw(
                "AVG( productivity) as productivity "
            ),
            DB::raw(
                "AVG( rate_of_attendance) as rate_of_attendance"
            ),
            "report_headers.line",
            "report_headers.report_date"
        ])->join("report_headers", "report_headers.id", "=", "operation_summaries.report_header_id")
            ->groupBy([
                "line", "report_headers.report_date"
            ]);

        if ($request->has('line')) {
            $query->where('report_headers.line', '=', $request->line);
        }

        /* if ($request->has('report_date')) {
            $query->where('report_headers.report_date', '=', $request->report_date);
        } */


        $queryResult = $query->get();
        $data = [];
        foreach ($queryResult as $key => $value) {
            # code...
            $value->{$value->line} = $value->productivity;
        }

        $results = collect([
            "request" => $request->all()
        ]);


        return $results->merge([
            "data" => $queryResult
        ]);
    }

    public function efficiencyPerDate(Request $request) {
        
        $query = DB::table('operation_summaries')->select([
            DB::raw("CAST(efficiency * 100 as numeric(8,2)) as efficiency"),
            DB::raw("CAST(rate_of_operation * 100 as numeric(8,2)) as rate_of_operation"),
            DB::raw("CAST(productivity * 100 as numeric(8,2)) as productivity"),
            DB::raw("CAST(rate_of_attendance * 100 as numeric(8,2)) as rate_of_attendance"),
            "report_headers.line",
            "report_headers.report_date",
            "group"
        ])->join("report_headers", "report_headers.id", "=", "operation_summaries.report_header_id")
        // ->groupBy([
        //     "line", "report_headers.report_date"
        // ])
        ;

        if ($request->has('line')) {
            if(is_array($request->line)) {
                $query->whereIn('report_headers.line', $request->line);
            }else{
                $query->where('report_headers.line', '=', $request->line);
            }
        }
        
        if ($request->has('start_date') && $request->has('end_date')) {
            $query->whereBetween('report_headers.report_date', [
                $request->get('start_date') , $request->get('end_date')
            ]);
        }else{
            $now = Carbon::now();
            $startdate = $now->firstOfMonth()->format("Y-m-d");
            $enddate = $now->endOfMonth()->format("Y-m-d");
            $query->whereBetween('report_headers.report_date', [
                $startdate , $enddate
            ]);
        }

        // return 
        $queryResult = $query
        ->orderBy('report_headers.report_date', 'asc')
        ->orderBy('report_headers.line', 'asc')
        ->get();

        $target = $this->getLineTarget($request, $queryResult );

        // return compact('queryResult', 'target');

        $queryResult = $queryResult->merge($target);

        return $this->formatForChart($queryResult, $request->type );
    }

    public function getLineTarget(Request $request, $queryResult) {
        $lines = $queryResult->pluck('line');
        $reportDates = $queryResult->pluck('report_date');
        
        $res = Line::select([
            'efficiency',
            'rate_of_operation',
            'productivity',
            'rate_of_attendance',
            DB::raw("lines.line + ' target' as line"),
            'report_headers.report_date',
            DB::raw('1 as [group]'),
        ])
        ->join("report_headers" , 'report_headers.line', '=', 'lines.line')
        ->whereIn('lines.line', $lines)
        ->whereIn('report_headers.report_date', $reportDates)
        ->get();

        return $res;
    }

    public function loss_time_per_dept(Request $request){
        
        $today = Carbon::now();
        $start_date = $request->has('start_date') && $request->start_date != 'null' ? $request->get('start_date') : $today->firstOfMonth()->format('Y-m-d');
        $end_date   = $request->has('end_date') && $request->end_date != 'null' ? $request->get('end_date') : $today->lastOfMonth()->format('Y-m-d');
        $query = "SELECT top 5
            b.dept_name as label,
            CAST( SUM( hours * man_power) AS numeric(8,2) ) as total
        FROM [production_report].[dbo].[loss_time_reports] a join dic b on a.dic = b.dept_code
            join report_headers c on a.report_header_id = c.id
        where c.report_date between '{$start_date}' and '{$end_date}'
        group by b.dept_name
        order by total desc";


        $data = DB::select($query);

        return [
            "success" => true,
            "params" => $request->all(),
            "data" => $data
        ];
        
    }

    public function loss_time_per_responsible(Request $request)
    {
        $today = Carbon::now();
        $start_date = $request->has('start_date') && $request->start_date != 'null' ? $request->get('start_date') : $today->firstOfMonth()->format('Y-m-d');
        $end_date   = $request->has('end_date') && $request->end_date != 'null' ? $request->get('end_date') : $today->lastOfMonth()->format('Y-m-d');
        $query = "SELECT top 5
            a.loss_code_name as label,
            CAST( SUM( hours * man_power) as numeric(8,2) ) as total
        FROM [production_report].[dbo].[loss_time_reports] a join dic b on a.dic = b.dept_code
            join report_headers c on a.report_header_id = c.id
        where c.report_date between '{$start_date}' and '{$end_date}'
        group by a.loss_code_name
        order by total desc";

        $data = DB::select($query);

        return [
            "success" => true,
            "params" => $request->all(),
            "data" => $data
        ];
    }
    
    public function loss_time_per_line(Request $request)
    {
        $today = Carbon::now();
        $start_date = $request->has('start_date') && $request->start_date != 'null' ? $request->get('start_date') : $today->firstOfMonth()->format('Y-m-d');
        $end_date   = $request->has('end_date') && $request->end_date != 'null' ? $request->get('end_date') : $today->lastOfMonth()->format('Y-m-d');
        $query = "SELECT top 5
            c.line as label,
            CAST( SUM( hours * man_power) as numeric(8,2) ) as total
        FROM [production_report].[dbo].[loss_time_reports] a join dic b on a.dic = b.dept_code
            join report_headers c on a.report_header_id = c.id
        where c.report_date between '{$start_date}' and '{$end_date}'
        group by c.line
        order by total desc";

        $data = DB::select($query);

        return [
            "success" => true,
            "params" => $request->all(),
            "data" => $data
        ];
    }

    public function least_avg_efficiency_perline(Request $request){
        $today = Carbon::now();
        $start_date = $request->has('start_date') && $request->start_date != 'null' ? $request->get('start_date') : $today->firstOfMonth()->format('Y-m-d');
        $end_date   = $request->has('end_date') && $request->end_date != 'null' ? $request->get('end_date') : $today->lastOfMonth()->format('Y-m-d');
        $query = "SELECT top 5
            b.line as label,
            CAST( AVG( a.[efficiency] ) as numeric(8,2) ) as total
        FROM [production_report].[dbo].[operation_summaries] a join report_headers b on a.report_header_id =  b.id
        where b.report_date between '{$start_date}' and '{$end_date}'
        group by b.line
        order by total asc ";

        $data = DB::select($query);

        return [
            "success" => true,
            "params" => $request->all(),
            "data" => $data
        ];
    }
    
    public function least_avg_productivity_perline(Request $request){
        $today = Carbon::now();
        $start_date = $request->has('start_date') && $request->start_date != 'null' ? $request->get('start_date') : $today->firstOfMonth()->format('Y-m-d');
        $end_date   = $request->has('end_date') && $request->end_date != 'null' ? $request->get('end_date') : $today->lastOfMonth()->format('Y-m-d');
        $query = "SELECT top 5
            b.line as label,
            CAST( AVG( a.[productivity] ) as numeric(8,2) ) as total
        FROM [production_report].[dbo].[operation_summaries] a join report_headers b on a.report_header_id =  b.id
        where b.report_date between '{$start_date}' and '{$end_date}'
        group by b.line
        order by total asc ";

        $data = DB::select($query);

        return [
            "success" => true,
            "params" => $request->all(),
            "data" => $data
        ];
    }
    
    public function least_avg_rate_of_attendance_perline(Request $request){
        $today = Carbon::now();
        $start_date = $request->has('start_date') && $request->start_date != 'null' ? $request->get('start_date') : $today->firstOfMonth()->format('Y-m-d');
        $end_date   = $request->has('end_date') && $request->end_date != 'null' ? $request->get('end_date') : $today->lastOfMonth()->format('Y-m-d');
        $query = "SELECT top 5
            b.line as label,
            CAST( AVG( a.[rate_of_attendance] ) as numeric(8,2) ) as total
        FROM [production_report].[dbo].[operation_summaries] a join report_headers b on a.report_header_id =  b.id
        where b.report_date between '{$start_date}' and '{$end_date}'
        group by b.line
        order by total asc ";

        $data = DB::select($query);

        return [
            "success" => true,
            "params" => $request->all(),
            "data" => $data
        ];
    }
    
    public function least_avg_rate_of_operation_perline(Request $request){
        $today = Carbon::now();
        $start_date = $request->has('start_date') && $request->start_date != 'null' ? $request->get('start_date') : $today->firstOfMonth()->format('Y-m-d');
        $end_date   = $request->has('end_date') && $request->end_date != 'null' ? $request->get('end_date') : $today->lastOfMonth()->format('Y-m-d');
        $query = "SELECT top 5
            b.line as label,
            CAST( AVG( a.[rate_of_operation] ) as numeric(8,2) ) as total
        FROM [production_report].[dbo].[operation_summaries] a join report_headers b on a.report_header_id =  b.id
        where b.report_date between '{$start_date}' and '{$end_date}'
        group by b.line
        order by total asc ";

        $data = DB::select($query);

        return [
            "success" => true,
            "params" => $request->all(),
            "data" => $data
        ];
    }

    public function formatForChart(Collection $data, $type = 'efficiency'){
        $res = [
            "labels" => [],
            "datasets" => [],
        ];
        $labelHashMap = [];
        $datasetHashMap = [];
        
        //   datasets: [
        //     {
        //       label: 'Data One',
        //       backgroundColor: '#f85179',
        //       data: [ 151,  159]
        //     }, {
        //       label: 'Data One',
        //       backgroundColor: '#f87979',
        //       data: [ 151,  189]
        //     }
        //   ]
        foreach ($data as $key => $value) {
            # code...
            if(!isset($labelHashMap[$value->report_date])) {
                $labelHashMap[$value->report_date] = 'visited';
                $res['labels'][] = $value->report_date;
            };

            if(!isset($datasetHashMap[$value->line . $value->group])) {
                $datasetHashMap[$value->line . $value->group] = [
                    "label" => "{$value->line} group {$value->group}",
                    "borderColor" => $this->initColor(),
                    // "backgroundColor" => "#ffffff",
                    "fill" => false,
                    "data" => [ $value->report_date => $value->{$type}]
                ];
            }else{
                $datasetHashMap[$value->line . $value->group]['data'][$value->report_date] = $value->{$type};
            }

        }
        
        $labels= [];
        // return $datasetHashMap;
        $newData = [];
        foreach ($labelHashMap as $report_date => $i) {
            # code...
            $labels[] = $report_date;

            foreach($datasetHashMap as $key => $dataSet) {
                if(!isset($newData[$key])) {
                    $newData[$key] = [];
                }

                if(isset($dataSet['data'][$report_date])) {
                    $newData[$key][] = number_format($dataSet['data'][$report_date], 2); //change to two place decimal
                    // (float) $dataSet['data'][$report_date]; // tgl label ada datanya;
                }else{
                    $newData[$key][] = 0; //karena tanggal label ga ada datanya
                }
            }
        }

        // return $newData;

        $datasets = [];
        foreach($datasetHashMap as $k => $value) {

            // $value['_count'] = count($newData[$k]); //ubah data ke newData
            $value['data'] = $newData[$k]; //ubah data ke newData
            $datasets[] = $value;
        }

        return [
            "_type" => $type,
            "labels" => $labels,
            'datasets' => $datasets
        ];

    }

    public function initColor(){
        return $this->random_color();
    }

    public function random_color_part()
    {
        return str_pad(dechex(mt_rand(0, 255)), 2, '0', STR_PAD_LEFT);
    }

    public function random_color()
    {
        return "#".$this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }
}
