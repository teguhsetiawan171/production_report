<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasterCategory;
use Illuminate\Support\Facades\DB;

class MasterCategoryController extends Controller
{
    public function combo(Request $request)
    {
        $data = MasterCategory::select([
            DB::raw('id as value'), DB::raw("category_name  as [text]")
        ])->get();

        return [
            'success' => true,
            'data' => $data
        ];
    }
}
