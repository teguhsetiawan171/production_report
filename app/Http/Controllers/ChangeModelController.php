<?php

namespace App\Http\Controllers;
use App\ChangeModelReport;
use App\ReportsHeader;
use Illuminate\Http\Request;


class ChangeModelController extends Controller
{
    public function index(){
       
        $data =ChangeModelReport::orderBy('id','desc')->get();
        $dropdown = ReportsHeader::get();
        return view('admin.vendor.change_model.index',compact('data','dropdown'));
    }

    public function create(Request $request){
       
        ChangeModelReport::create($request->all());
        return redirect('/admin/change_model');
    }
}
