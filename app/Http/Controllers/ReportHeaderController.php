<?php

namespace App\Http\Controllers;

use App\ReportHeader;
use Illuminate\Http\Request;


class ReportHeaderController extends CrudController
{
    protected $model = "App\ReportHeader";

    public function all()
    {
        return ReportHeader::all();
    }

    public function store(Request $request)
    {
        $params = $this->validate($request, [
            'line' => 'required',
            'report_date' => 'required|date'
        ]);

        $header = ReportHeader::firstOrCreate($params, [
            'created_by' => $request->created_by
        ]);

        return [
            'success' => true,
            'data' => $header,
        ];
    }

    public function show($id)
    {
        return ReportHeader::find($id);
    }

    public function update(Request $request, $id)
    {
        $reportHeader = ReportHeader::find($id);
        $reportHeader->update($request->all());
        return $reportHeader;
    }

    public function delete($id)
    {
        // get report ID
        $reportHeader = ReportHeader::find($id);

        // soft delete. dengan cara penambahan kolom activated di report header. boolean 0 1, 0 is actived and 1 is deleted
    }
}
