<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ChangeMemberReportController extends CrudController
{
    protected $model = 'App\\ChangeMemberReport';
}
