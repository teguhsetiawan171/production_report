<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CrudModel;

class ReportHeaderList extends CrudModel
{
    protected $table = 'report_headers';
    protected $fillable = ['line', 'report_date'];
}
