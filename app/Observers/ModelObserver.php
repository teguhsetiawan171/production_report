<?php

namespace App\Observers;

use App\Log;
use App\CrudModel;
use Illuminate\Support\Facades\Auth;

class ModelObserver
{

    public function saved(CrudModel $model)
    {
        $table = $model->getTable();
        $data = $model->toJson();
        $user = is_null(Auth::user()) ? "unknown" : Auth::user()->name;
        (new Log)->info("{$table} is saved by {$user}. {$data}");
    }

    public function deleting(CrudModel $model)
    {
        $table = $model->getTable();
        $data = $model->toJson();
        $user = is_null(Auth::user()) ? "unknown" : Auth::user()->name;

        (new Log)->info("{$table} is deleted by {$user}. {$data}");
    }
}
