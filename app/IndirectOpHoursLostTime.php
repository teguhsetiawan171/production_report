<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IndirectOpHoursLostTime extends CrudModel
{
    //
    protected $table = 'loss_time_operation_summaries';
}
