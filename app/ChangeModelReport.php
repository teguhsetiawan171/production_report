<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CrudModel;

class ChangeModelReport extends CrudModel
{
    protected $table = "change_model_reports";

    public function setTotalTimeAttribute($value)
    {
        // hours = time end - time start 
        // 3600 = detik dalam satu jam
        $result = (\strtotime($this->attributes['time_end']) - \strtotime($this->attributes['time_start'])) / 60;

        if ($result < 0) {
            $oneday = (24 * 60 * 60); //in second
            $result = (
                (\strtotime($this->attributes['time_end']) + $oneday) - \strtotime($this->attributes['time_start'])) / 60;
        }

        $this->attributes['total_time'] = $result;
    }
}
