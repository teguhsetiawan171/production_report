<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => NULL,
                'role_id' => 1,
                'name' => 'Admin',
                'email' => 'admin@admin.com',
                'avatar' => 'users/default.png',
                'password' => '$2y$10$5hgfc1PanJiUVEwSnE3YlO/4sU440f4BWz/Rny0ziFvU9faF1FNTK',
                'line' => NULL,
                'access_level' => 3,
                'remember_token' => 'm5UHGn5aCMlzoXG9y3mkGIFBld9eKmAMYfVibVDBx86YnTCEVKjPaxFnTTcS',
                'settings' => NULL,
                'created_at' => '2020-05-05 03:09:35',
                'updated_at' => '2020-05-05 03:09:35',
            ),
        ));
        
        
    }
}