<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReportHeaderIdToOpSummaries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('operation_summaries', function (Blueprint $table) {
            //
            $table->unsignedInteger('report_header_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('operation_summaries', function (Blueprint $table) {
            //
            $table->unsignedInteger('report_header_id');
        });
    }
}
