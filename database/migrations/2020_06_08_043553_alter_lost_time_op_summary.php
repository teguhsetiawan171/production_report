<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLostTimeOpSummary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loss_time_operation_summaries', function (Blueprint $table) {
            //
            $table->unsignedInteger('master_lost_time_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loss_time_operation_summaries', function (Blueprint $table) {
            //
            $table->dropColumn('master_lost_time_id');
        });
    }
}
