<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChangeMemberReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('change_member_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('report_header_id')->unsigned();
            $table->string('is_change', 10)->default('NO');
            $table->string('shift', 10);
            $table->string('refrence_no')->nullable();
            $table->string('remark', 255)->nullable();
            $table->string('created_by', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('change_member_reports');
    }
}
