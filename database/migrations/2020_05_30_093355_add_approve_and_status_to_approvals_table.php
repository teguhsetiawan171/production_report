<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddApproveAndStatusToApprovalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('approvals', function (Blueprint $table) {
            $table->dateTime('approval_time', 0)->nullable()->after('report_id');
            $table->string('user_approve')->nullable()->after('approval_time');
            $table->integer('status')->nullable()->after('user_approve');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('approvals', function (Blueprint $table) {
            $table->dropColumn('approval_time');
            $table->dropColumn('user_approve');
            $table->dropColumn('status');
        });
    }
}
