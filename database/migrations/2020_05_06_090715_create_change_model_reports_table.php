<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChangeModelReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('change_model_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('report_header_id')->unsigned();
            $table->string('prev_model', 50);
            $table->string('time_start', 50);
            $table->string('next_model', 50);
            $table->string('time_end', 50);
            $table->float('total_time');
            $table->unsignedInteger('normal_minute')->nullable();
            $table->unsignedInteger('abnormal_minute')->nullable();
            $table->string('major_problem', 255)->nullable();
            $table->string('pic', 50)->nullable();
            $table->timestamps();
            $table->string('created_by', 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('change_model_reports');
    }
}
