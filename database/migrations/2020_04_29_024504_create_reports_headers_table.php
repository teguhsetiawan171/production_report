<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_headers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('line', 25);
            $table->date('schedule_date')->nullable(); //tanggal schedule seharusnya running
            $table->date('report_date'); //hari tanggal report itu diisi
            $table->integer('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_headers');
    }
}
